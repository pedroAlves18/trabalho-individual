package com.a18694.chatapp.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.a18694.chatapp.R
import com.a18694.chatapp.model.User
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import java.util.*
import kotlin.reflect.KClass

class LoginActicity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    private val email: EditText by lazy{
        findViewById<EditText>(R.id.login_email_et)
    }

    private val password: EditText by lazy{
        findViewById<EditText>(R.id.login_password_et)
    }

    private val openRegister: TextView by lazy{
        findViewById<TextView>(R.id.login_register_tv)
    }

    private val buttonLogin: TextView by lazy{
        findViewById<TextView>(R.id.login_login_btn)
    }

    private var checkError: Boolean = false
    private lateinit var user: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_acticity)
        auth = FirebaseAuth.getInstance()
        val checkLoggedUser: FirebaseUser? = auth.currentUser
        if(checkLoggedUser!= null){
            openMainActivity()
        }

        //open Register activity
        openRegister.setOnClickListener {
            openRegisterActivity()
        }

        //login btn
        buttonLogin.setOnClickListener{view ->
            if(!email.text.toString().isEmpty() && !password.text.toString().isEmpty()) {
                if(isEmailValid(email.text.toString())){

                    val user = User()
                    user.email = email.text.toString()
                    user.password = password.text.toString()
                    loggingIn(user);
                }else
                    Toast.makeText(this, R.string.em_validEmail, Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(this, R.string.em_fillAllField, Toast.LENGTH_SHORT).show()
            }
        }
    }
    // validate email address
    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    //login function
    private fun loggingIn(user: User){
        auth.signInWithEmailAndPassword(user.email, user.password).addOnCompleteListener(this) { task: Task<AuthResult> ->
            if (task.isSuccessful) {
                openMainActivity()
            } else {
                try {
                    throw task.getException()!!;
                } catch (e: FirebaseAuthInvalidUserException) {
                    Toast.makeText(this, R.string.em_emailNotFound, Toast.LENGTH_LONG).show();
                    e.printStackTrace()
                } catch (e: FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(this, R.string.em_dontMatch, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (e: Exception) {
                    Toast.makeText(this, R.string.em_loginError, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }

    private fun openMainActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
    private fun openRegisterActivity(){
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}
