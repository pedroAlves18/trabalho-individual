package com.a18694.chatapp.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.a18694.chatapp.R
import com.a18694.chatapp.model.User
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import com.a18694.chatapp.config.ConfigFirebase as ConfigFirebase

class RegisterActivity : AppCompatActivity() {

    private val name: EditText by lazy{
        findViewById<EditText>(R.id.register_name_et)
    }
    private val email: EditText by lazy{
        findViewById<EditText>(R.id.register_email_et)
    }
    private val password: EditText by lazy{
        findViewById<EditText>(R.id.register_password_et)
    }
    private val confirmPass: EditText by lazy{
        findViewById<EditText>(R.id.register_confPassword_et)
    }
    private var checkError: Boolean = false

    private lateinit var user: User

    private lateinit var auth:FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val btnBack = findViewById<Button>(R.id.register_back_btn)
        btnBack.setOnClickListener {
            finish()
        }

        val btnRegister = findViewById<Button>(R.id.register_singUp_btn)
        btnRegister.setOnClickListener{
            //check if exist any field empty
            if(isEmptyField(name.text.toString()) || isEmptyField(email.text.toString()) || isEmptyField(password.text.toString()) || isEmptyField(confirmPass.text.toString())){
                Toast.makeText(this,R.string.em_fillAllField,Toast.LENGTH_SHORT).show()
                checkError = true 
            }

            //confirm matching passwords
            if(!confirmPass.text.toString().equals(password.text.toString())){
                Toast.makeText(this,R.string.em_matchPassword,Toast.LENGTH_SHORT).show()
                checkError = true
            }
            //check if exist any error type
            if(!checkError){
                val user = User()
                user.name = name.text.toString();
                user.email = email.text.toString();
                user.password = password.text.toString();
                saveUserAuth(user)
            }
        }
    }
    private fun isEmptyField(field: String): Boolean {
        return field.isEmpty();
    }

    private fun saveUserAuth(user: User){
        auth = FirebaseAuth.getInstance()
        auth.createUserWithEmailAndPassword(user.email, user.password).addOnCompleteListener(this) {task:Task<AuthResult> ->
            if (task.isSuccessful) {
                Toast.makeText(this, R.string.signUpSucess,Toast.LENGTH_LONG).show();
                finish();
            }
            else{
                try {
                    throw task.getException()!!;
                }catch (e: FirebaseAuthWeakPasswordException){
                    Toast.makeText(this, R.string.em_weakPassword, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }catch (e: FirebaseAuthInvalidCredentialsException){
                    Toast.makeText(this, R.string.em_validEmail, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }catch (e: FirebaseAuthUserCollisionException){
                    Toast.makeText(this, R.string.em_existentAccount, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }catch (e: Exception){
                    Toast.makeText(this, R.string.em_signUpUser, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        }
    }
}
