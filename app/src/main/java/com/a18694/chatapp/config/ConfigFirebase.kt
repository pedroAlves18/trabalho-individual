package com.a18694.chatapp.config

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

public class ConfigFirebase {

    lateinit var database: DatabaseReference
    lateinit var auth: FirebaseAuth

    //return FirebaseDatabase instance
    fun getFirebaseDatabase():DatabaseReference {
        if(database == null)
            database = FirebaseDatabase.getInstance().getReference()
        return database
    }

    //return FirebaseAuth instance
    fun getFirebaseAuth(): FirebaseAuth{
        if(auth == null)
            auth = FirebaseAuth.getInstance()
        return auth
    }

}